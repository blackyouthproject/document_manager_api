<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailSentDateColumnToDownloadRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('download_request', function (Blueprint $table) {
            $table->timestamp('email_sent_date')->nullable()->after('email_sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('download_request', function (Blueprint $table) {
            $table->dropColumn('email_sent_date');
        });
    }
}
