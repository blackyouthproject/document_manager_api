<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnToDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('document', function (Blueprint $table) {
			$table->unsignedTinyInteger('status')->default("1")->nullable()->after('month');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('document', function (Blueprint $table) {
			$table->dropColumn('status');
		});
    }
}
