<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchedulingColumnsToDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document', function (Blueprint $table) {
            $table->timestamp('scheduled_for')->nullable()->after('downloads');
            $table->timestamp('published_on')->nullable()->after('scheduled_for');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('download', function (Blueprint $table) {
            $table->dropColumn('scheduled_for');
            $table->dropColumn('published_on');
        });
    }
}
