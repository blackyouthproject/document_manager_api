<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('download_request');
        Schema::create('download_request', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('document');
            $table->unsignedInteger('requester');
            $table->unsignedInteger('package');

//            $table->foreign('document')
//                ->references('id')
//                ->on('document')
//                ->onDelete('cascade');
//            $table->foreign('package')
//                ->references('id')
//                ->on('package')
//                ->onDelete('cascade');
//            $table->foreign('requester')
//                ->references('id')
//                ->on('requester')
//                ->onDelete('cascade');

            $table->tinyInteger('approved')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('download_request');
    }
}
