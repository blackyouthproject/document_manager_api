<?php

use \Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class AddDocumentStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::table('status')->insert(
			[
				'name' => 'published',
				'description' => 'This document has been published.',
				'created_at' => \Carbon\Carbon::now(),
				'updated_at' => \Carbon\Carbon::now(),
				'deleted_at' => '0000-00-00 00:00:00'
			]);

		DB::table('status')->insert(
			[
				'name' => 'scheduled',
				'description' => 'This document has been scheduled.',
				'created_at' => \Carbon\Carbon::now(),
				'updated_at' => \Carbon\Carbon::now(),
				'deleted_at' => '0000-00-00 00:00:00'
			]);

		DB::table('status')->insert(
			[
				'name' => 'embargoed',
				'description' => 'This document is embargoed.',
				'created_at' => \Carbon\Carbon::now(),
				'updated_at' => \Carbon\Carbon::now(),
				'deleted_at' => '0000-00-00 00:00:00'
			]);

	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::table('document_type')->where('name', 'published')->delete();
		DB::table('document_type')->where('name', 'scheduled')->delete();
		DB::table('document_type')->where('name', 'embargoed')->delete();
    }
}
