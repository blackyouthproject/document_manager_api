<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewTypesToDocumentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('document_type')->insert(['name' => 'report', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'dataset', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'factsheet', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'byp-report', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'press-release', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'topline', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'codebook', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
        DB::table('document_type')->insert(['name' => 'field-report', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now(), 'deleted_at' => '0000-00-00 00:00:00']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('document_type')->where('name', 'report')->delete();
        DB::table('document_type')->where('name', 'dataset')->delete();
        DB::table('document_type')->where('name', 'factsheet')->delete();
        DB::table('document_type')->where('name', 'byp-report')->delete();
        DB::table('document_type')->where('name', 'press-release')->delete();
        DB::table('document_type')->where('name', 'topline')->delete();
        DB::table('document_type')->where('name', 'codebook')->delete();
        DB::table('document_type')->where('name', 'field-report')->delete();
    }
}
