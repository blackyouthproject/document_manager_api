<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->string('wp_id');
            $table->string('wp_user_id');
            $table->string('title');
            $table->string('file_name');
            $table->integer('type_id');
            $table->longText('text');
            $table->longText('excerpt');
            $table->tinyInteger('month');
            $table->tinyInteger('year');
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement('ALTER TABLE document ADD FULLTEXT search(title, excerpt, text)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document', function($table) {
	    	$table->dropIndex('search');
		});
        Schema::dropIfExists('document');
    }
}
