<?php

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'first_name'    => $faker->firstName,
        'last_name'     => $faker->lastName,
        'username'      => $faker->userName,
        'email'         => $faker->email,
        'password'      => Illuminate\Support\Facades\Hash::make($faker->password),
    ];
});
