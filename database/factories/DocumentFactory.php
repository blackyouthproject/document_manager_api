<?php

$factory->define(App\Models\Document::class, function (Faker\Generator $faker) {
    return [
        'wp_id'         => $faker->randomNumber(),
        'wp_user_id'    => $faker->randomNumber(),
        'title'         => $faker->title,
        'file_name'     => str_replace(' ', '_', strtolower($faker->words(intval(rand(1,5)), true))),
        'type_id'       => $faker->randomDigitNotNull,
        'text'          => $faker->paragraphs(10,true),
        'excerpt'       => $faker->paragraph(3, true),
        'month'         => $faker->dayOfMonth(),
        'year'          => $faker->year(),
    ];
});
