<?php

return [
    'wp' => [
        'url' => env('WP_URL', null),
    ],
];
