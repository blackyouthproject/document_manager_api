<?php
return [
    '001'   => [
            'message'       => 'Invalid download link.',
            'redirect_url'  => 'http://genforwardsurvey.com/',
        ],
    '002'   => [
            'message'       => 'Invalid document.',
            'redirect_url'  => 'http://genforwardsurvey.com/',
        ],
    '003'   => [
            'message'       => 'Invalid request id.',
            'redirect_url'  => 'http://genforwardsurvey.com/',
        ],
    '004'   => [
            'message'       =>  'Could not find requester information.',
            'redirect_url'  => 'http://genforwardsurvey.com/',
        ],
];
