# GenForward Survey Document Manager API

The GenForward Survey Document Manager API maintains a store of documents that can be searched and downloaded on any consuming website. It also maintains a store of download reqeusts and download requesters for each document. It is authenticated by JWT tokens that are only available to first-party apps.

## Setup
1. Clone this repository to your local environment.
2. Create a local database for this project.
3. Create an .env with your credentials.
4. Run `composer install`.
5. Run `php artisan migrate`.
6. Run `php artisan key:generate`.
7. Run `php artisan serve`.

## Usage
1. Once your local environment is up and running, register a new user with a post request to `api.local/auth/register`
   - first_name
   - last_name
   - username
   - email
   - password
2. Once you're registered, get a new token with a post request to `api.local/auth/login`
   - email
   - password
3. Pass the token you get from Step #2 to each endpoint as a query variable, i.e. `api.local/document/?token=TOKEN`

## Test
