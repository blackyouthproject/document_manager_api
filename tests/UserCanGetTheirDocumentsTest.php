<?php

use Illuminate\Support\Facades\Hash;

class UserCanGetTheirDocumentsTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanGetTheirDocuments()
    {
        // create a fake user
        $faker = Faker\Factory::create();
        $password = $faker->password;
        $user = factory(App\Models\User::class)->make([
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'username'      => $faker->userName,
            'email'         => $faker->email,
            'password'      => Hash::make($password),
        ]);
        // create a fake document for the user
        $document = factory(App\Models\Document::class)->make();
        $document->save();

        // update the user id
        $user->id = $document->wp_user_id;
        $user->save();
        // login to get a token for the user
        $data = [
            'email'     => $user->email,
            'password'  => $password,
        ];
        $token = json_decode($this->json('POST', '/auth/login', $data)->response->getContent())->token;
        $this->get('/document?token=' . $token);
        $response = json_decode($this->response->getContent(), true);
        $this->seeStatusCode(200)
            ->seeJsonStructure(['documents']);
        $this->assertArraySubset(['id' => $document->id], $response['documents'][0]);
    }

    public function testUserCannotGetAnotherUsersDocuments()
    {
        // create a fake user
        $faker = Faker\Factory::create();
        $password = $faker->password;
        $user1 = factory(App\Models\User::class)->make([
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'username'      => $faker->userName,
            'email'         => $faker->email,
            'password'      => Hash::make($password),
        ]);
        $document1 = factory(App\Models\Document::class)->make();
        $document1->save();
        $user1->id = $document1->wp_user_id;
        $user1->save();

        $user = factory(App\Models\User::class)->make([
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'username'      => $faker->userName,
            'email'         => $faker->email,
            'password'      => Hash::make($password),
        ]);
        $document = factory(App\Models\Document::class)->make();
        $document->save();
        $user->id = $document->wp_user_id;
        $user->save();
        // login to get a token for the user
        $data = [
            'email'     => $user->email,
            'password'  => $password,
        ];
        $token = json_decode($this->json('POST', '/auth/login', $data)->response->getContent())->token;
        $this->get('/document?token=' . $token);
        $response = json_decode($this->response->getContent(), true);
        reset($response['documents']);
        $first_key = key($response['documents']);
        $this->assertFalse(intval($response['documents'][$first_key]['wp_user_id']) === intval($user1->id));
    }
}
