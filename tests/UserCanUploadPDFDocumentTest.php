<?php

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserCanUploadPDFDocumentTest extends TestCase
{
    public function testUserCanUploadAPDFDocument()
    {
        // create a fake user
        $faker = Faker\Factory::create();
        $password = $faker->password;
        $user = factory(App\Models\User::class)->make([
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'username'      => $faker->userName,
            'email'         => $faker->email,
            'password'      => Hash::make($password),
        ]);
        $user->save();
        // log the user in
        $token = JWTAuth::attempt(['email'=> $user->email, 'password'=>$password]);
        // create a fake file to upload
        Storage::fake('documents');

        $response = $this->json('POST', '/document', [
            'document' => UploadedFile::fake()->create('document.pdf', 10000),
            'wp_user_id' => strval($user->id),
            'token' => $token,
        ]);
        $this->seeStatusCode(200)->seeJsonStructure(['data']);

        $response = json_decode($response->response->getContent(), true);
        Storage::disk('documents')->assertExists($response['data']['file_name']);
        // create a fake document for the user
//        $document = factory(App\Models\Document::class)->make();
//        $document->save();

        // update the user id
//        $user->id = $document->wp_user_id;
        // login to get a token for the user
//        $data = [
//            'email'     => $user->email,
//            'password'  => $password,
//        ];
//        $token = json_decode($this->json('POST', '/auth/login', $data)->response->getContent())->token;
    }
}
