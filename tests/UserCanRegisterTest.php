<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserCanRegisterTest extends TestCase
{
    /**
     * Test the endpoints
     *
     * @return void
     */
    public function testUserCanViewRegistrationPage()
    {
        $this->get('/auth/register');
        $data = json_decode($this->response->getContent(), true);
        $this->assertArrayHasKey('message', $data);
        $this->assertEquals('register', $data['message']);
    }

    public function testRegistrationDataValidates()
    {
        $data = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password',
        ];
        $validation = [
            'first_name' => ['The first name field is required.'],
            'last_name' => ['The last name field is required.'],
            'username' => ['The username field is required.'],
            'email' => ['The email field is required.'],
            'password' => ['The password field is required.'],
        ];
        $response = json_decode($this->json('POST', '/auth/register', $data)->response->getContent(), true);
        $this->assertEquals($validation, $response);
    }

    public function testUserCanRegister()
    {
        $faker = Faker\Factory::create();
        $data = [
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'username'      => $faker->userName,
            'email'         => $faker->email,
            'password'      => $faker->password,
        ];
        $this->json('POST', '/auth/register', $data)
            ->seeStatusCode(200)
            ->seeJsonStructure([
            'user' => [
                'id',
                'first_name',
                'last_name',
                'username',
                'email',
                'updated_at',
                'created_at',
            ],
        ]);
    }
}
