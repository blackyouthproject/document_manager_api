<?php

use Illuminate\Support\Facades\Hash;

class UserCanLoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserCanLogin()
    {
        // create a fake user
        $faker = Faker\Factory::create();
        $password = $faker->password;
        $user = factory(App\Models\User::class)->make([
            'first_name'    => $faker->firstName,
            'last_name'     => $faker->lastName,
            'username'      => $faker->userName,
            'email'         => $faker->email,
            'password'      => Hash::make($password),
            ]);
        $user->save();
        // log the user in
        $data = [
            'email'     => $user->email,
            'password'  => $password,
        ];
        $this->json('POST', '/auth/login', $data)
            ->seeStatusCode(200)
            ->seeJsonStructure(['token']);
    }
}
