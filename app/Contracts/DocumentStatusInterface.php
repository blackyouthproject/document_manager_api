<?php
namespace App\Contracts;

use App\Models\DocumentStatus;

interface DocumentStatusInterface
{
    public function get($id);

    public function save(DocumentStatus $type);

    public function edit(DocumentStatus $type, array $data);

    public function all();

    public function delete();

}
