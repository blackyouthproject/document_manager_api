<?php
namespace App\Contracts;

use App\Models\Requester;

interface RequesterInterface
{
    public function get($id);

    public function save(array $data);

    public function edit($id, array $data);

    public function all();

    public function delete();

}
