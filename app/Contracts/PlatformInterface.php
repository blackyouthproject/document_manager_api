<?php
namespace App\Contracts;

use App\Models\Platform;

interface PlatformInterface
{
    public function get($id);

    public function save(Platform $platform);

    public function edit(Platform $platform, array $data);

    public function all();

    public function delete();

}
