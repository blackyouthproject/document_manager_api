<?php
namespace App\Contracts;

interface DownloadRequestInterface
{
    public function get(int $id);

    public function save(array $data);

    public function edit($id, array $data);

    public function all();

    public function delete(int $id);

    public function validate(string $hash);
}
