<?php
namespace App\Contracts;

use App\Models\DocumentType;

interface DocumentTypeInterface
{
    public function get($id);

    public function save(DocumentType $type);

    public function edit(DocumentType $type, array $data);

    public function all();

    public function delete();

}
