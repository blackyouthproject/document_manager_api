<?php
namespace App\Contracts;

use App\Models\Package;

interface PackageInterface
{
    public function get($id);

    public function save(Package $package);

    public function edit(Package $package, array $data);

    public function all($user_id);

    public function delete();
}
