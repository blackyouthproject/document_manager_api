<?php
namespace App\Contracts;

use Illuminate\Http\UploadedFile;
use App\Models\Document;

interface DocumentInterface
{
    public function get($id);

    public function download($id);

    public function save(UploadedFile $document, bool $isPDF = true);

    public function edit(int $id, Document $document, array $data);

    public function all(array $filters = []);

    public function delete(int $id);

    public function getAbsolutePath(Document $document);
}
