<?php

namespace App\Services;

use App\Models\Requester;
use App\Contracts\RequesterInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RequesterService implements RequesterInterface
{
    protected $data;

    public function get($id)
    {
        try {
            return Request::where('id', $id)
                ->where('deleted_at', null)
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    public function save(array $data)
    {
        $this->data = $data;
        // if the requester email exists, return that requester
        $requester = Requester::where('email', $this->data['email'])->first();
        if ($requester) {
            return $requester;
        }
        // persist the information
        $requester = new Requester();
        $requester->title = isset($this->data['title']) ? $this->data['title'] : 'None given';
        $requester->first_name = $this->data['first_name'] ?: 'None given';
        $requester->last_name = $this->data['last_name'] ?: 'None given';
        $requester->institution = isset($this->data['institution']) ? $this->data['institution'] : 'None given';
        $requester->purpose = isset($this->data['purpose']) ? $this->data['purpose'] : 'None given';
        $requester->email = isset($this->data['email']) ? $this->data['email'] : 'None given';

        $requester->save();
        return $requester;
    }

    public function edit($id, array $data)
    {
        $requester = Requester::find($id);
        if(!$requester) {
            return false;
        }
        $requester->last_name = isset($data['last_name']) ? $data['last_name'] : $requester->last_name;
        $requester->first_name = isset($data['first_name']) ? $data['first_name'] : $requester->first_name;
        $requester->institution = isset($data['institution']) ? $data['institution'] : $requester->institution;
        $requester->title = isset($data['title']) ? $data['title'] : $requester->title;
        return $requester->save();
    }

    public function all()
    {
        if (!$user_id) {
            return Requester::all()->where('deleted_at', null);
        }
        return Requester::all()->where('deleted_at', null);
    }

    public function delete()
    {
        $requester = $this->get($id);
        if ($requester) {
            $requester->deleted_at = date('Y-m-d H:i:s');
            return $requester->save();
        }
    }
}