<?php

namespace App\Services;

use App\Models\DocumentStatus;
use App\Contracts\DocumentStatusInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DocumentStatusService implements DocumentStatusInterface
{
    public $data;

    public function setData($data)
    {
        $this->data = $data;
    }

    public function get($id)
    {
        try {
            return DocumentStatus::where('id', $id)
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
           return false;
        }
    }

    public function edit(DocumentStatus $documentStatus, array $data)
    {
        $documentStatus->name = isset($data['name']) ? $data['name'] : $documentStatus->name;
        return $documentStatus->save();
    }

    public function save(DocumentStatus $documentStatus)
    {
        // TODO: implement save method
    }

    public function all()
    {
        return DocumentStatus::all();
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}