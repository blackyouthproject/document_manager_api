<?php

namespace App\Services;

use App\Models\DocumentType;
use App\Contracts\DocumentTypeInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DocumentTypeService implements DocumentTypeInterface
{
    public $data;

    public function setData($data)
    {
        $this->data = $data;
    }

    public function get($id)
    {
        try {
            return DocumentType::where('id', $id)
                ->where('deleted_at', '0000-00-00 00:00:00')
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
           return false;
        }
    }

    public function edit(DocumentType $documentType, array $data)
    {
        $documentType->name = isset($data['name']) ? $data['name'] : $documentType->name;
        return $documentType->save();
    }

    public function save(DocumentType $documentType)
    {
        // TODO: implement save method
    }

    public function all()
    {
        return DocumentType::all();
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}