<?php

namespace App\Services;

use App\Models\Document;
use Illuminate\Http\UploadedFile;
use App\Contracts\DocumentInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use \Smalot\PdfParser\Parser as Parser;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DocumentService implements DocumentInterface
{
	const PUBLISHED = 1;
	const SCHEDULED = 2;
	const EMBARGOED = 3;
    public $data;

    public function setData($data)
    {
        $this->data = $data;
    }

    public function get($id)
    {
        try {
            return Document::where('id', $id)
                ->where(function ($query) {
                    $query->where('deleted_at', '0000-00-00 00:00:00')
                        ->orWhere('deleted_at', null);
                })
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
           return false;
        }
    }

    public function edit(int $id, Document $document, array $data)
    {
        Log::info(var_export($data, true));
        $document->wp_user_id = isset($data['wp_user_id']) ? $data['wp_user_id'] : $document->wp_user_id;
        $document->title = isset($data['title']) ? $data['title'] : $document->title;
        $document->type_id = isset($data['type_id']) ? $data['type_id'] : $document->type_id;
        $document->platform_id = isset($data['platform_id']) ? $data['platform_id'] : $document->platform_id;
		$document->status = isset($data['status']) ? $data['status'] : $document->status;
        $document->excerpt = isset($data['excerpt']) ? $data['excerpt'] : $document->excerpt;
        $document->month = isset($data['month']) ? $data['month'] : $document->month;
        $document->year = isset($data['year']) ? $data['year'] : $document->year;
        $document->scheduled_for = isset($data['scheduled_for']) ? $data['scheduled_for'] : $document->scheduled_for;
        return $document->save();
    }

    public function save(UploadedFile $file, bool $isPDF = true)
    {
        // make sure the file is valid
        if (!$file->isValid()) {
            return false;
        }
        // move the file to the proper storage
        $basename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $filename = "{$basename}_" . time() . '.' . $file->getClientOriginalExtension();
        $path = '/app/' . $file->storeAs('documents', $filename, ['disk' => 'local']);

        // parse the file
        $parsed = $isPDF ? $this->parseFile( storage_path() . $path) : false;
        // persist the information
        $document = new Document();
        if (isset($this->data['wp_user_id'])) {
            $document->wp_user_id = $this->data['wp_user_id'];
        } else {
            $document->wp_user_id = Auth::user()->id;
        }
        if (isset($this->data['wp_id'])) {
            $document->wp_id = $this->data['wp_id'];
        }
        $excerpt = $parsed ? $parsed['excerpt'] : '';
        $document->title = (isset($this->data['title']) && $this->data['title'] !== '') ? $this->data['title'] : $basename;
        $document->file_name = $filename;
        $document->type_id = isset($this->data['type_id']) ? $this->data['type_id'] : '';
		$document->status = isset($this->data['status']) ? $this->data['status'] : 1;
        $document->platform_id = isset($this->data['platform_id']) ? $this->data['platform_id'] : '';
        $document->text = $parsed ? $parsed['text'] : '';
        $document->excerpt = (isset($this->data['excerpt']) && $this->data['excerpt'] !== '') ? $this->data['excerpt'] : $excerpt;
        $document->month = isset($this->data['month']) ? $this->data['month'] : '';
        $document->year = isset($this->data['year']) ? $this->data['year'] : '';
        if (isset($this->data['scheduled_for'])) {
            $document->scheduled_for = $this->data['scheduled_for'];
        }
        if (isset($this->data['published_on'])) {
            $document->published_on = $this->data['published_on'];
        }
        if (isset($this->data['deleted_at'])) {
            $document->deleted_at = $this->data['deleted_at'];
        }
        if (isset($this->data['deleted_at'])) {
            $document->deleted_at = $this->data['deleted_at'];
        }
        if (isset($this->data['deleted_at'])) {
            $document->deleted_at = $this->data['deleted_at'];
        }
        Log::info(var_export($this->data['excerpt'], true));
        Log::info(isset($this->data['title']));

        $document->save();
        return $document;
    }

    public function parseFile($file)
    {
        $parsed = [
            'text' => '',
            'excerpt' => '',
        ];
        $parser = new Parser();
        $pdf = $parser->parseFile($file);
        $parsed['text'] = $pdf->getText();
        $parsed['excerpt'] = substr($parsed['text'], 0, 300);
        return $parsed;
    }

    public function all(array $filters = [])
    {
        DB::connection('mysql')->enableQueryLog();
        $query = Document::whereNull('deleted_at')
            ->select(
                [
                    'id',
                    'wp_id',
                    'wp_user_id',
                    'title',
                    'file_name',
                    'status',
                    'type_id',
                    'platform_id',
                    'excerpt',
                    'month',
                    'year',
                    'downloads',
                    'scheduled_for',
                    'published_on',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                ]);
        if (!empty($filters)) {
            $query->whereIn('type_id', $filters);
        }
        $results = $query->get();
        $queries = DB::getQueryLog();
        Log::info('Logging queries: ');
        Log::info(var_export($queries, true));
        return $results;
    }

    public function delete(int $id)
    {
        $document = $this->get($id);
        if ($document) {
            $document->deleted_at = date('Y-m-d H:i:s');
            return $document->save();
        }
    }

    public function download($id)
    {
        $document = $this->get($id);
        $path = $this->getAbsolutePath($document);
        // https://stackoverflow.com/a/46049403
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$document->file_name.'"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path)); //Absolute URL
        ob_clean();
        flush();
        readfile($path); //Absolute URL
        exit();
    }

    public function getAbsolutePath(Document $document)
    {
        return storage_path() . '/app/documents/' . $document->file_name;
    }
}