<?php

namespace App\Services;

use App\Models\Document;
use App\Models\DownloadRequest;
use App\Contracts\DownloadRequestInterface;
use App\Models\Requester;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DownloadRequestService implements DownloadRequestInterface
{
    protected $data;

    public function get(int $id)
    {
        try {
            return DownloadRequest::select(
                'download_request.id as request',
                'download_request.document as document',
                'download_request.package as package',
                'download_request.approved as approved',
                'download_request.email_sent as email_sent',
                'download_request.email_sent_date as email_sent_date',
                'download_request.created_at as date_request_made',
                'requester.id as requester',
                'requester.first_name',
                'requester.last_name',
                'requester.email',
                'requester.title',
                'requester.institution',
                'requester.purpose'
                )
                ->where('download_request.id', $id)
                ->where('download_request.deleted_at', null)
                ->join('requester', 'download_request.requester', '=', 'requester.id')
                ->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return false;
        }
    }

    public function save(array $data)
    {
        $this->data = $data;
        if (!$this->data['requester']) {
            return false;
        }
        // persist the information
        $request = new DownloadRequest();
        $request->document = isset($this->data['document']) ? $this->data['document'] : null;
        $request->requester = $this->data['requester'];
        $request->package = isset($this->data['package']) ? $this->data['package'] : 0;

        $request->save();
        return $request;
    }

    public function edit($id, array $data)
    {
        $this->data = $data;
        $request = DownloadRequest::find($id);
        if(!$request) {
            return false;
        }
        $request->document = isset($this->data['document']) ? $this->data['document'] : $request->document;
        $request->requester = isset($this->data['requester']) ? $this->data['document'] : $request->requester;
        $request->package = isset($this->data['package']) ? $this->data['package'] : $request->package;
        $request->approved = isset($this->data['approved']) ? $this->data['approved'] : $request->approved;
        $result = $request->save();

        return $result ? $request->getRequestDetails() : false;
    }

    public function all()
    {
        $request = DB::table('download_request')
            ->select(
                'download_request.id as request',
                'download_request.document as document',
                'download_request.package as package',
                'download_request.approved as approved',
                'download_request.created_at as date_request_made',
                'requester.id as requester',
                'requester.first_name',
                'requester.last_name',
                'requester.email',
                'requester.title',
                'requester.institution',
                'requester.purpose',
                'document.title as doc_title'
            )
            ->where('download_request.deleted_at', null)
            ->join('document', 'download_request.document', '=', 'document.id')
            ->join('requester', 'download_request.requester', '=', 'requester.id')->get();
        return $request;
    }

    public function delete(int $id)
    {
        $request = $this->get($id);
        if ($request) {
            $request->deleted_at = date('Y-m-d H:i:s');
            return $request->save();
        }
    }

    private function validateDocument($document_id)
    {
        return Document::where('id', $document_id)
            ->where('deleted_at', null)
            ->orWhere('deleted_at', '0000-00-00 00:00:00')
            ->first();
    }

    private function validateRequest($request_id)
    {
        return DownloadRequest::where('id', $request_id)
            ->where('deleted_at', null)
            ->first();
    }

    private function validateRequester($requester_id)
    {
        return Requester::where('id', $requester_id)
            ->where('deleted_at', null)
            ->first();
    }

    public function validate(string $hash)
    {
        $results = [
            'valid' => false,
        ];
        if (!$hash){
            $results['error'] = [
                'code'  => 002,
                'message' => config('error.001.message')
            ];
        }
        // a token is a hash of the DOCUMENT_ID-REQUEST_ID-REQUESTER_ID
        $request = Crypt::decryptString($hash);
        $request = explode('-', $request);
        if(!$this->validateDocument((int) $request[0])) {
            $results['error'] = [
                'code'  => 002,
                'message' => config('error.002.message')
            ];
            return $results;
        }

        if(!$this->validateRequest((int) $request[1])) {
            $results['error'] = [
                'code'  => 003,
                'message' => config('error.003.message')
            ];
            return $results;
        }

        if(!$this->validateRequester((int) $request[2])) {
            $results['error'] = [
                'code'  => 004,
                'message' => config('error.004.message')
            ];
            return $results;
        }

        $results['valid'] = true;
        $results['id'] = $request[0];
        
        return $results;
    }
}