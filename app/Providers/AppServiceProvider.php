<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Tymon\JWTAuth\Providers\LumenServiceProvider::class);
        $this->app->bind('App\Contracts\DocumentInterface', 'App\Services\DocumentService');
        $this->app->bind('App\Contracts\DocumentTypeInterface', 'App\Services\DocumentTypeService');
		$this->app->bind('App\Contracts\DocumentStatusInterface', 'App\Services\DocumentStatusService');
        $this->app->bind('App\Contracts\RequesterInterface', 'App\Services\RequesterService');
        $this->app->bind('App\Contracts\DownloadRequestInterface', 'App\Services\DownloadRequestService');
    }
}
