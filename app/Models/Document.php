<?php

namespace App\Models;

class Document extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wp_id',
        'wp_user_id',
        'title',
        'file_name',
        'type_id',
        'platform_id',
        'text',
        'excerpt',
        'month',
        'year',
        'downloads',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function type() {
        return $this->belongsTo('App\Models\DocumentType');
    }

    public function platform() {
        return $this->belongsTo('App\Models\Platform');
    }
}
