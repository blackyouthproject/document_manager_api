<?php

namespace App\Models;

class Package extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'package';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'requester_id',
    ];

    public function requester()
    {
        return $this->belongsTo('App\Models\Requester');
    }

    public function documents() {
        return $this->hasMany('App\Models\Document');
    }
}
