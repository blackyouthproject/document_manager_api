<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class DownloadRequest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'download_request';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'document',
        'requester',
        'package',
        'approved',
        'deleted_at'
    ];

    public function getRequestDetails()
    {
        return DB::table('download_request')
            ->select(
                'download_request.id as request',
                'download_request.document as doc',
                'download_request.package as package',
                'download_request.approved as approved',
                'download_request.created_at as date_request_made',
                'download_request.email_sent as email_sent',
                'download_request.email_sent_date as email_sent_date',
                'requester.id as requester',
                'requester.first_name',
                'requester.last_name',
                'requester.email',
                'requester.title',
                'requester.institution',
                'requester.purpose',
                'document.title as doc_title'
            )
            ->where('download_request.deleted_at', null)
            ->where('download_request.id', $this->id)
            ->join('document', 'download_request.document', '=', 'document.id')
            ->join('requester', 'download_request.requester', '=', 'requester.id')
            ->get();
    }
}
