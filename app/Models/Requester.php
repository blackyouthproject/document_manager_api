<?php

namespace App\Models;

class Requester extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'requester';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'title',
        'institution',
        'purpose',
        'deleted_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function packages() {
        return $this->hasMany('App\Models\Packages');
    }
}
