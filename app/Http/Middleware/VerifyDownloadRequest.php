<?php

namespace App\Http\Middleware;

use App\Services\DownloadRequestService;
use Closure;

class VerifyDownloadRequest
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $download = new DownloadRequestService();
        if ($request->input('f') == "true" && !$request->has('ts')) {
            return $next($request);
        }

        if (!$request->has('ts')) {
            return redirect(config('gfs.wp.url') . '/error');
        }

        $isValid = $download->validate($request->input('ts'));
        if ($isValid['valid'] === false) {
            return redirect(config('error' . $isValid['error']['code'] . 'redirect_url'));
        }

        $id = $isValid['id'];
        return redirect(env('APP_URL') . '/download/' . $id);
    }

}