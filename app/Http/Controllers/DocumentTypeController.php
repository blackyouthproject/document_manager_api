<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\DocumentTypeInterface;

class DocumentTypeController extends Controller
{
    private $documentType;


    /**
     * DocumentController constructor.
     * @param DocumentTypeInterface $documentType
     */
    public function __construct(DocumentTypeInterface $documentType)
    {
        $this->middleware('auth');
        $this->documentType = $documentType;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
        ]);
    }

    public function index(Request $request)
    {
        $types = $this->documentType->all();
        return response()->json(compact('types'));
    }

    public function edit(Request $request, $id)
    {
        $documentType = $this->documentType->get($id);
        if(!$documentType) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Document type not found.',
                ]
            ], 404);
        }

        $result = $this->documentType->edit($documentType, $request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not update document type.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Document type updated.',
            ]
        ], 200);
    }

    public function view($id)
    {
        $document = $this->document->get($id);
        if(!$document) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Document not found.',
                ]
            ], 404);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
            ],
            'document' => $document,
        ], 200);
    }
}