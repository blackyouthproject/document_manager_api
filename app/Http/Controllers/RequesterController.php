<?php

namespace App\Http\Controllers;

use App\Models\Requester;
use Illuminate\Http\Request;
use App\Contracts\RequesterInterface;
use Illuminate\Support\Facades\Log;

class RequesterController extends Controller
{
    public $requester;

    public function __construct(RequesterInterface $requester)
    {
        $this->middleware('auth');
        $this->requester = $requester;
    }

    public function index(Request $request)
    {
        $result = Requester::all()->where('deleted_at', null);
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not retrieve requesters.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Requesters retrieved.',
            ],
            'data' => $result,
        ], 200);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
        ]);

        $result = $this->requester->save($request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not create requester.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Requester created.',
            ],
            'data' => $result,
        ], 200);
    }

    public function edit(int $id, Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
        ]);

        $result = $this->requester->edit($id, $request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not update requester\'s details',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Requester updated.',
            ],
            'data' => $result,
        ], 200);

    }

    public function view($id)
    {
        $requester = $this->requester->get($id);
        if(!$requester) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Requester not found.',
                ]
            ], 404);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
            ],
            'requester' => $requester,
        ], 200);

    }

    public function delete($id)
    {
        $requester = $this->requester->delete($id);
        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Requester deleted.',
            ],
            'data' => $requester,
        ], 200);
    }
}
