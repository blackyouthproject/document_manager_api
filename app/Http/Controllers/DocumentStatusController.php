<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\DocumentStatusInterface;

class DocumentStatusController extends Controller
{
    private $documentStatus;


    /**
     * DocumentController constructor.
     * @param DocumentStatusInterface $documentStatus
     */
    public function __construct(DocumentStatusInterface $documentStatus)
    {
        $this->middleware('auth');
        $this->documentStatus = $documentStatus;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
        ]);
    }

    public function getStatus(Request $request)
    {
        $statuses = $this->documentStatus->all();
        return response()->json(compact('statuses'));
    }

    public function edit(Request $request, $id)
    {
        $documentStatus = $this->documentStatus->get($id);
        if(!$documentStatus) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Document status not found.',
                ]
            ], 404);
        }

        $result = $this->documentStatus->edit($documentStatus, $request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not update document status.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Document status updated.',
            ]
        ], 200);
    }

    public function view($id)
    {
        $document = $this->document->get($id);
        if(!$document) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Document not found.',
                ]
            ], 404);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
            ],
            'document' => $document,
        ], 200);
    }
}