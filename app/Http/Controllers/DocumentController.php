<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Contracts\DocumentInterface;
use Illuminate\Support\Facades\Log;

class DocumentController extends Controller
{
    private $document;


    /**
     * DocumentController constructor.
     * @param DocumentInterface $document
     */
    public function __construct(DocumentInterface $document)
    {
        $this->middleware('auth', ['except' => ['download']]);
        $this->document = $document;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'document' => 'required|file',
            'wp_user_id' => 'required|digits',
        ]);
    }

    public function getDocument(Request $request)
    {
        Log::info('User #' . Auth::user()->id . ' requests all documents.');
        $types = $request->get('types', null);
        $types = is_null($types) ? [] : explode(',', $types);
        $documents = $this->document->all($types);
        return response()->json(compact('documents'));
    }

    public function download(int $id, Request $request)
    {
        $this->document->download($id);
        exit();
    }

    public function upload(Request $request)
    {
        Log::info('Uploading a new document...');
		Log::info(var_export($request->all(), true));
        // validate post body
        $this->validate($request, [
            'document'      => 'required|file',
            'wp_user_id'    => 'required|numeric',
            'title'         => 'required',
            'type_id'       => 'required',
			'status'        => 'required',
            'month'         => 'required',
            'year'          => 'required',
        ]);
        Log::info(var_export($request->all(), true));
        if (!$request->hasFile('document')
            || !$request->file('document')->isValid()
        ) {
            Log::info('Invalid document');
            Log::info('Document exists: ' . var_export($request->hasFile('document'), true));
            Log::info('Document is valid:' . var_export(!$request->file('document')->isValid(), true));
            Log::info('Document is pdf: ' . var_export($request->document->extension() !== 'pdf', true));
            Log::info($request->document->extension());
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Invalid document.',
                ]
            ], 422);
        }
        Log::info('Attempting to save uploaded document');
        $isPDF = $request->document->extension() === 'pdf';
        $this->document->setData($request->all());
        $file = $this->document->save($request->file('document'), $isPDF);

        if(!$file) {
            Log::info('Unable to upload document');
            return response()->json([
                'errors'  => [
                    'status'  => 500,
                    'message' => 'Unable to upload document.',
                ]
            ], 500);
        }

        $data = [
            'id'            => $file->id,
            'year'          => $file->year,
            'title'         => $file->title,
            'month'         => $file->month,
            'excerpt'       => $file->excerpt,
            'type_id'       => $file->type_id,
            'file_name'     => $file->file_name,
			'status'        => $file->status,
            'wp_user_id'    => $file->wp_user_id,
            'platform_id'   => $file->platform_id,
        ];
        Log::info('Document uploaded!');
        Log::info(var_export($data, true));
        return response()->json(compact('data'));
    }

    public function edit(Request $request, $id)
    {
        $document = $this->document->get($id);
        if(!$document) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Document not found.',
                ]
            ], 404);
        }

        $result = $this->document->edit($id, $document, $request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not update document.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Document updated.',
            ],
            'data' => $document,
        ], 200);
    }

    public function view($id)
    {
        $document = $this->document->get($id);
        if(!$document) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Document not found.',
                ]
            ], 404);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
            ],
            'document' => $document,
        ], 200);
    }

    public function delete($id)
    {
        $document = $this->document->delete($id);
        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Document deleted.',
            ],
            'data' => $document,
        ], 200);
    }
}