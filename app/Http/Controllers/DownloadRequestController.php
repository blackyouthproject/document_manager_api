<?php

namespace App\Http\Controllers;

use App\Models\DownloadRequest;
use App\Services\DocumentService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Contracts\DownloadRequestInterface;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Mailjet\Client as MailJet;
use Mailjet\Resources;

class DownloadRequestController extends Controller
{
    public $download_request;

    public function __construct(DownloadRequestInterface $download_request)
    {
        $this->middleware('auth');
        $this->download_request = $download_request;
    }

    public function index()
    {
        $result = $this->download_request->all();
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not retrieve download requests.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Requests retrieved.',
            ],
            'data' => $result,
        ], 200);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'requester' => 'required',
            'document' => 'required',
        ]);

        $result = $this->download_request->save($request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not create a download request.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Download request created.',
            ],
            'data' => $result,
        ], 200);
    }

    public function edit(int $id, Request $request)
    {
        Log::info('Editing Request #' . $id);
        $this->validate($request, [
            'id' => 'required',
        ]);

        $result = $this->download_request->edit($id, $request->all());
        if(!$result) {
            return response()->json([
                'errors'  => [
                    'status'  => 422,
                    'message' => 'Could not update download request.',
                ]
            ], 422);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Download request updated.',
            ],
            'data' => $result,
        ], 200);

    }

    public function view($id)
    {
        $download_request = $this->download_request->get($id);
        if(!$download_request) {
            return response()->json([
                'errors'  => [
                    'status'  => 404,
                    'message' => 'Download request not found.',
                ]
            ], 404);
        }

        return response()->json([
            'success'  => [
                'status'  => 200,
            ],
            'data' => $download_request,
        ], 200);

    }

    public function delete($id)
    {
        $download_request = $this->download_request->delete($id);
        return response()->json([
            'success'  => [
                'status'  => 200,
                'message' => 'Download request deleted.',
            ],
            'data' => $download_request,
        ], 200);
    }

    /**
     * Approves the incoming request
     * @param int $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function approve(int $id, Request $request)
    {
        $downloadRequest = $this->download_request->get($id);
        if ($downloadRequest) {
            $downloadRequest = $downloadRequest->toArray();
            $approve = $this->download_request->edit($id, ['approved' => 1]);
            $document = new DocumentService();
            $document = $document->get($downloadRequest['document']);
            if ($document) {
                $document = $document->toArray();
                Log::info(var_export($document, true));
                Log::info(var_export($downloadRequest, true));
                Log::info($downloadRequest['document'] . "-" . $downloadRequest['request'] . "-" . $downloadRequest['requester']);
                $link = Crypt::encryptString($downloadRequest['document'] . "-" . $downloadRequest['request'] . "-" . $downloadRequest['requester']);
                $downloadRequest['link'] = env('APP_URL') . '/?ts=' . $link;
                $client = new Mailjet(env('MAILJET_KEY'), env('MAILJET_SECRET'), true, ['version' => 'v3.1']);
                $body = [
                    'Messages' => [
                        [
                            'From' => [
                                'Email' => "admin@admin.genforwardsurvey.com",
                                'Name' => "GenForward Survey"
                            ],
                            'To' => [
                                [
                                    'Email' => $downloadRequest['email'],
                                    'Name' => $downloadRequest['first_name'] . " " . $downloadRequest['last_name']
                                ]
                            ],
                            'Subject' => "[GenForwardSurvey] Download Request Approved",
                            'TextPart' => "Your request to download {$document['title']} has been approved. You can download it at the following link: {$downloadRequest['link']}",
                            'HTMLPart' => "Your request to download <em>{$document['title']}</em> has been approved. You can download it at the following link: <br />{$downloadRequest['link']}",
                        ]
                    ]
                ];
                $response = $client->post(Resources::$Email, ['body' => $body]);
                if ($response->success()) {
                    Log::info(var_export($response->getData(), true));
                    return response()->json([
                        'success' => [
                            'status' => 200,
                            'message' => 'Download request approved.',
                        ],
                        'data' => ['link' => $downloadRequest['link']],
                    ], 200);
                } else {
                    return response()->json([
                        'success' => [
                            'status' => 400,
                            'message' => 'Download request approval failed.',
                        ],
                        'data' => [
                            'message' => $response->getReasonPhrase()
                        ],
                    ], 400);
                }
            }
        } else {
            return response()->json([
                'success' => [
                    'status' => 400,
                    'message' => 'Invalid download request',
                ],
                'data' => [
                    'message' => 'This download request does not exist.'
                ],
            ], 400);
        }
    }
}
