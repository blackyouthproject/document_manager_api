<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
$router->get('/', function (Request $request) {
    $data = $request->all();
    if (empty($data['ts'])) {
        return redirect('http://genforwardsurvey.com');
    }

    $download = new App\Services\DownloadRequestService();
    $isValid = $download->validate($request->input('ts'));

    if ($isValid['valid'] === false) {
        return redirect('http://genforwardsurvey.com');
    }

    $id = $isValid['id'];
    return redirect(env('APP_URL') . '/download/' . $id . '?f=true');
});

$router->post('/auth/login', 'AuthController@login');
$router->post('/auth/register', 'RegisterController@postRegister');
$router->get('/auth/register', 'RegisterController@getRegister');

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/document', 'DocumentController@getDocument');
    $router->post('/document', 'DocumentController@upload');
    $router->post('/document/{id}', 'DocumentController@edit');
    $router->get('/document/{id}', 'DocumentController@view');
    $router->delete('/document/{id}', 'DocumentController@delete');

    $router->get('/type', 'DocumentTypeController@index');
	$router->get('/status', 'DocumentStatusController@getStatus');

    $router->get('/requester', 'RequesterController@index');
    $router->post('/requester', 'RequesterController@create');
    $router->post('/requester/{id}', 'RequesterController@edit');
    $router->get('/requester/{id}', 'RequesterController@view');
    $router->delete('/requester/{id}', 'RequesterController@delete');

    $router->get('/download-request', 'DownloadRequestController@index');
    $router->post('/download-request', 'DownloadRequestController@create');
    $router->post('/download-request/{id}', 'DownloadRequestController@edit');
    $router->post('/download-request/approve/{id}', 'DownloadRequestController@approve');
    $router->get('/download-request/{id}', 'DownloadRequestController@view');
    $router->delete('/download-request/{id}', 'DownloadRequestController@delete');
});

$router->group(['middleware' => 'jwt.refresh'], function () use ($router) {
    $router->get('/refresh', 'AuthController@refresh');
});

$router->group(['middleware' => 'verify_download_request'], function () use ($router) {
    $router->get('/download/{id}', 'DocumentController@download');
});
