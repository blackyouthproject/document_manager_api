IWE: an API for Documents
Document
    schema
        belongsTo one Platform
        belongsTo one DocumentType
        belongsTo many Packages
        hasMany Downloads
    endpoints
        create
        read
        update
        destroy
        upload
        download
Package
    schema
        hasMany Documents
        hasOne Requestor
    endpoints
        create
        read
        update
        destroy
        download
DocumentType
    schema
        hasMany Documents
    endpoints
        create
        read
        update
        destroy
Platform
    schema
        hasMany Documents
    endpoints
        create
        read
        update
    destroy
Messages
    schema
        belongsTo one User
    endpoints
        create
        read
        send
        update
        destroy
Requestor
    schema
        hasMany Packages
        hasMany Messages
    endpoints
        create
        read
        update
        destroy
Request
    schema
    endpoints
        create
        read
        update
        destroy
        approve
        deny
Download
    schema
        hasOne Document
    endpoints
        create
        read
        update
        destroy